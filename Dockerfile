FROM ubuntu
RUN apt-get update -y && apt-get install openssh-client -y
RUN mkdir -p ~/.ssh
RUN chmod 700 ~/.ssh
